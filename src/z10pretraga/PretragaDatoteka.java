package z10pretraga;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PretragaDatoteka {
	
	static String[] explodedInput;
	static String[] explodedFrase;

	public static void main(String[] args) {
		
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Unesi lokaciju pretrage:"); 
		String folder = scanner.nextLine(); 										//unos imena i �irine tablice
		System.out.println("Unesi pojam ili frazu pretrage"); 
		String frase = scanner.nextLine(); 
		
		scanner.close();
		
		searchFolder(folder, frase);
		
	}
	
	static void searchFolder(String path, String frase) {									//funkcija pretra�uje zadani folder
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();											//sve datoteke iz zadanog foldera se spremaju u polje

		for (int i = 0; i < listOfFiles.length; i++) {										//for petlja prolazi kroz sve datoteke
		  File file = listOfFiles[i];
		  if (file.isFile() && file.getName().endsWith(".txt")) {							//ako datoteka ima sufiks .txt
		      if (searchForFrase(readFileToString(file), "prve detalje")) {						//funkcijom readFileToString njen sadr�aj �ita u string
		    	  System.out.println(file.getName());
		      }
		  } 
		}
	}
	
	
	static String readFileToString(File file) {												//funkcija pretvara File u String
		String toReturn = "";
		
		try {																				//try blok, potreban zbog scannera
		      Scanner myReader = new Scanner(file);
		      while (myReader.hasNextLine()) {												//za svaku liniju teksta iz datoteke
		        toReturn += myReader.nextLine() + " ";										//dodaje ju u string koji �e na kraju vratiti
		      }
		      myReader.close();
		    } catch (FileNotFoundException e) {System.out.println("Greska");}
		
		return toReturn;
	}
	
	
	static boolean searchForFrase(String txt, String frase) {
		
		explodedInput = txt.split(" ");
		explodedFrase = frase.split(" ");
																						//funkcija zapocinje rekurzivnu funkciju checkNextWord za svaku rijec iz teksta
		for (int i = 0; i < explodedInput.length - explodedFrase.length + 1; i++) {		//osim ako je fraza duza od ostatka teksta, pa je postojanje nemoguce 
			if (checkNextWord(i, 0)) {return true;}
		}
		return false;
	}
	
	static boolean checkNextWord(int inputPos, int frasePos) {					//rekurzivna funkcija za usporedivanje fraza s tekstom
		if (explodedInput[inputPos].equals(explodedFrase[frasePos])) {			//usporedba pojedinacne rijeci teksta s pojedinacnom rijeci iz fraze, nije casesensitive
			if (explodedFrase.length == frasePos + 1) return true;				//provjera je li rekurzija dosla do zadnje rijeci fraze
			else return checkNextWord(inputPos +  1, frasePos + 1);				//ako je: vra�a true, ako nije: rekurzija se nastavlja za iducu rijec teksta i fraze
		}
		else return false;														//fraza ne postoji u tekstu
	}
}
