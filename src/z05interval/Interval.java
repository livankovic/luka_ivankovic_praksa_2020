/*

Pretpostavio sam da se "broja�" iz zadatka ne odnosi na broja� for petlje za prolazak kroz interval

*/
package z05interval;

public class Interval {

	public static void main(String[] args) {
		
		int intervalStart = 3;
		int intervalEnd = 150;
		
		int counter = 0;
		
		if (intervalStart >= 10 || intervalEnd <= 100) { //provjera ispravnosti intervala
			System.out.println("Neispravno definiran interval");
		}
		else
		{
			for (int i = intervalStart; i < intervalEnd; i++) { //prolazak kroz interval
				if (i <= 15) { //o	ukoliko je broj manji ili jednak 15, broja� treba pove�ati za 5
					counter += 5;
				}
				if (i > 15) { //o	ukoliko je broj ve�i od 15, broja� treba umanjiti za 1
					counter--;
				}
				if (i % 20 == 0) {} //o	ukoliko je broj djeljiv s 20, treba prekinuti procesuiranje tog broja i prije�i na sljede�i, else if u idu�em redu
				else if (i == 75) { //o	ukoliko je broj jednak 75, treba prekinuti procesuiranje intervala te ispisati vrijednost broja�a 
					break; //prekid for petlje
				}
			}
			System.out.println(counter); //ispis broja�a
		}
	}

}
