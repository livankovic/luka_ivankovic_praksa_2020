package z06tablicaMnozenja;

import java.util.Scanner;

public class TablicaMnozenja {
	
	public static int width; 											//sirina tablice, do kojeg broja
	public static String name; 											//ime na kraju
	public static String title = "TABLICA  MNOZENJA"; 					//naslov tablice
	
	public static int maxNumOfDigits; 									//broj znamenki najveceg moguceg broja u tablici
	public static int rowWidth; 										//�irina jednog reda u znakovima
	
	public static String[] buffer; 										//buffer za ispis

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Unesi svoje ime:"); 
		name = scanner.nextLine(); 										//unos imena i �irine tablice
		System.out.println("Unesi �eljenu �irinu tablice: (5-100)"); 
		width = Integer.parseInt(scanner.nextLine());
		
		maxNumOfDigits = String.valueOf(width*width).length(); 			//broj znamenku najveceg moguceg broja
		rowWidth = (width+1) * (maxNumOfDigits+1) + 1;					//sirina jednog reda u znakovima
		
		buffer = new String[width + 8];									//buffer za konacni ispis tablice
		
		addHeader();													//dodaje header tablice, prvih 5 redova sa naslovom i ukrasima
		populateTable();												//popunjava tablicu s umnoscima
		addFooter();													//dodaje podno�je tablice s potpisom i ukrasima, zadnja 3 reda
		printBuffer();													//ispisuje kona�ni buffer
																		//addHeader, populateTable i addFooter mogu biti pozvane bilo kojim redoslijedom 
		scanner.close();
	}

	
	static void addHeader() {
		int numOfColons = (rowWidth - (title.length()+2))/4;
		
		buffer[0] = addLine();
		buffer[1] = ": ".repeat(numOfColons) + " " + title + " " + " :".repeat(numOfColons);					//dodaje naslov tablice u i ukrase u buffer
		buffer[2] = addLine();
		buffer[3] = " ".repeat(maxNumOfDigits-1)+"* |" + getTableRow(1);										//dodaje horizontalnu os tablice u buffer
		buffer[4] = addLine();
	}
	
	static void addFooter() {
		String signature = "by " + name;
		
		
		buffer[5+width] = addLine();
		buffer[6+width] = ": ".repeat(rowWidth).substring(0, rowWidth-signature.length()) + signature;			//dodaje ukrase i potpis na kraj tablice
		buffer[7+width] = addLine();
	}
	
	static String addLine() { 																					//dodaje punu liniju cijelom sirinom
		return "-".repeat(rowWidth);
	}
	
	static String getTableRow(int row) {																		//funkcija vraca zeljeni red tablice kao ispravno formatirani string
		String toReturn = "";
		for (int i = 0; i < width; i++) {
			int currentNumber = row*(i+1);
			toReturn += " ".repeat((maxNumOfDigits-String.valueOf(currentNumber).length()+1)) + currentNumber;	//dodaje ispravan broj razmaka prije broja
		}
		return toReturn;
	}
	
	static void populateTable() {																				//funkcija poziva funkciju getTableRow za svaki potrebni redak tablice
		for (int i = 1; i < width+1; i++) {
			int bufferIndex = i + 4;
			buffer[bufferIndex] = " ".repeat(maxNumOfDigits-String.valueOf(i).length())+ i +" |" + getTableRow(i);
		}
	}
	
	static void printBuffer() {																					//ispisuje cijeli buffer
		for (int i = 0; i < width+8; i++) {
			System.out.println(buffer[i]);
		}
	}
}
