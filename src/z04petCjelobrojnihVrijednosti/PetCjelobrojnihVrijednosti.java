package z04petCjelobrojnihVrijednosti;

public class PetCjelobrojnihVrijednosti {

	public static void main(String[] args) {
		
		int[] intArray = {10, 11, 133, 1102, 8};
		
		for (int i = 0; i < 5; i++){
			
			int currentNumber = intArray[i];
			
			boolean even = currentNumber % 2 == 0; //broj je cjelobrojan ako je ostatak cjelobrojnog djeljenja sa 2 jednak 0
			
			if (even) System.out.println(currentNumber + " je paran broj");
			else System.out.println(currentNumber + " nije paran broj");
		}
	}
			
}
