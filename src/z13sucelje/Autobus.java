package z13sucelje;

interface vozilo {
	public void playEngineStartSound();
	public void playEngineStopSound();
}

public class Autobus implements vozilo {
	public void playEngineStartSound() {
		System.out.println("BRRRRMMMMMMM");
	}
	public void playEngineStopSound() {
		System.out.println("TSSSSSSS");
	}
}
